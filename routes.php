<?php
/**
 * All routes is registered here
 */
//Auths
$router->get('admin/register','AuthsController@register');  //Registration page
$router->post('admin/register','AuthsController@store_registered_user');    //Store registered user's details
$router->get('admin/home','AuthsController@home');  //Redirect to the home page
$router->get('admin/login','AuthsController@login');    //Login page
$router->post('admin/login','AuthsController@validate_user_login'); //Validate logging in user
$router->get('admin/logout','AuthsController@logout');  //End Session
//Business 
$router->get('admin/businesses/create','BusinessesController@create');  //Create New Business
$router->post('admin/businesses','BusinessesController@store'); //Store Business informations
$router->get('admin/businesses','BusinessesController@index');  //Show all registered businesses

//Categories
$router->get('admin/categories/create','CategoriesController@create');  //Create new Categories
$router->get('admin/categories','CategoriesController@index');  //Show all categories
$router->post('admin/categories','CategoriesController@store'); //Store category informations
//Businesses Modification
$router->get('admin/businesses/edit','BusinessesController@edit');//Edit business informations
$router->post('admin/businesses/edit','BusinessesController@update');   //Update the changes
$router->get('admin/businesses/delete','BusinessesController@destroy');//Delete Business
//Categories Modification
$router->get('admin/categories/delete','CategoriesController@destroy');    //Delete a category
$router->get('admin/businesses/add_category','CategoriesController@addToBusiness'); //Add Categories To business
$router->post('admin/businesses/add_category','CategoriesController@storeBusinessCategory'); //Store category

//Query Tests Goes Here

$router->get('query','queryTesterController@index');    //Query Builder Test

//Public page
$router->get('business','PublicController@index');  //Show the public landing page
$router->get('business/search','PublicController@search');  //Search results
$router->get('business/search/result','PublicController@result');   //Search Target viewed

//Add Images To Business
$router->get('admin/business/image/add','BusinessesController@addImages');  //Add image to the business
$router->post('admin/business/image/add','BusinessesController@storeImages');   //Store image to the business