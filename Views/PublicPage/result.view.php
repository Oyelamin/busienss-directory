<!DOCTYPE html>
<html>  
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Search Result</title>
    <link href="/Public/assets/css/bulma.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <style>
      body{
        background:linear-gradient(rgba(255,255,258,0.5),rgba(255,255,255,3.20)),url('/Public/img/bac12.jpg');
      }
      .card-header{
        display: flex;
        flex-wrap: nowrap;
        overflow-x: auto;
        -webkit-overflow-scrolling: touch;
        scroll-behavior: smooth;
        transition: .8s;
      }
      .card-header::-webkit-scrollbar{
        display:none;
        transition: .8s;
      }
    .images{

    }
    .cards{
      box-shadow:4px 4px 40px pink;
    }
    </style>
    <!--
      * Node JS
      * Vue JS
      * Angular
      * React
    -->
  </head>
  <body>

  <br><br><br>
  <div class="container">
  <h1 style="color:white;text-shadow:3px 4px 5px black;font-size:50px;margin-bottom:-50px;"><b>BUSINESS</b></h1>
  </div>
  <section class="section">
    
    <div class="container">
    <?php foreach($results as $business): ?>
          <div style="cursor:move;" class="cards">
            <div class="card">
              <header class="card-header">
                <?php foreach($images as $image): ?>
                <img style="cursor:zoom-in;height:300px;border:2px dotted green;" src="/Public/uploads/img/<?php echo $image->image; ?>">
                <?php endforeach; ?>
              </header>
              <div style="border:1px solid green;" class="card-content">
                <div class="content">
                <h1 class="h2"><?php echo $business->name ?> <a style="float:right;" class="button is-success is-rounded is-small"><?php echo $views;?> Views</a> </h1> <hr>
                <small><?php echo $business->about ?></small>
                  <hr>
                  <b><p>Categories: </p></b>
                  <?php  
                    $query = require 'core/bootstrap.php';
                    $id = $business->id;
                    $categories = $query->selectJoin($business->id);
                    foreach($categories as $category):
                  ?>
                  <small style="border-right:1px solid silver;padding:4px;" class="small"><?php echo $category->category;?></small>
                    <?php endforeach; ?>
                  <br>
                  <b><p>Location: </p></b>
                  <small>
                    <?php echo 'No '.$business->street_number.', '.$business->street_name.', '.$business->city.', '.$business->country ?>.
                  </small>
                  <b><p>Phone:</p></b>
                  <small><?php echo $business->phone ?></small>
                  <b><p>Email Address: </p></b>
                  <small><?php echo $business->email ?></small>
                    <hr>
                   
                </div>
              </div>
              
            </div>
          </div>
        <?php endforeach; ?>
    </div>
  </section>
  <script>
    
  </script>
  </body>
</html>