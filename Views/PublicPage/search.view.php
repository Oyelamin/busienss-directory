<!DOCTYPE html>
<html>  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Search Results</title>
    <link href="../Public/assets/css/bulma.css" rel="stylesheet">
    <link href="../Public/assets/css/animate.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <style>
        body{
            
            background-size:cover;
            background-attachment:fixed;
            height:100%;
        }
        .no-result{
            font-size:40px;
        }
        .f-box{
            text-align:center;
            font-weight:bolder;
            font-size:20px;
            border-radius:0px;
            color:white;
            text-shadow:2px 3px 2px black;
            background:url(/Public/img/bac3.jpg);

            
        }
        #notification{
            /* box-shadow:4px 4px 20px purple ; */
            text-align:center;
        }
    </style>
  </head>
  <body>
  <div class="box f-box">
            <h1 class="h1 button is-danger is-rounded is-medium animated bounceInDown""> <b>You have <?php echo count($results); ?> Results for your Search!</b> </h1>
        </div>
  <section class="section animated fadeInRight">
  
    <div class="container">
        <a class="button is-link is-medium is-rounded" href="javascript:void(0)" style=""><h1 style="font-size:30px"><b>SEARCH (-></b> <?php echo $search; ?>?</h1></a><hr>
        <?php
            if(count($results) >= 1):
            foreach($results as $result):
        ?>
                <div style="border-radius:0px;" class="box">
                    <h1>
                        <a href="/business/search/result?name=<?php echo $result->name; ?>"><b><?php echo $result->name; ?></b></a>
                    </h1><hr>
                    <p><?php echo $result->about ?></p>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <br><br>
            <div id="notification" class="notification is-danger animated bounceInRight">
                <h1 class="no-result">Ooops, No Result Found For Your Search!</h1>
            </div>
        <?php endif ?>
    </div>
  </section>
  <br><br><br>
  </body>
</html>