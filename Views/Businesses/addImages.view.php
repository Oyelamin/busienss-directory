<!DOCTYPE html>
<html>  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Add image</title>
    <link href="../Public/assets/css/bulma.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
    <!-- <link href="/Public\assets\css\bootstrap.min.css" rel="stylesheet" /> -->
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <style>

        .box{
            box-shadow:4px 4px 20px palegreen;
        }
    </style>
  </head>
  <body>
  <?php view('Layouts/header'); ?>
  <br><br><br><br><br>
  <section class="section">
    <div class="container">
        <div class="box">
            <div class="box-header">
                <h1 class="h1">
                <i class="fa fa-plus"></i> ADD NEW IMAGES
                </h1>
                <hr>
                <?php view('Layouts/Validation'); ?>
                <form action="" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo $id;?>">
                    <label for="" class="label">SELECT IMAGE</label>
                    
                    <div id="attachment" class="animated bounceInLeft" style="display:;">
                    
                    <div class="input-group control-group increment">
                        <div class="input">
                        <input type="file" name="a_file" class="input">
                            <button class="btn-success button"  style="font-weight:bold;" type="button"><i class="fa fa-plus"></i>Add</button>
                        </div>
                    </div>
                    <!-- <div class="clone hide" style="display:none;">
                        <div class="control-group input-group" style="margin-top:10px">
                            <div class="input">
                            <input type="file" name="a_file" class="input">
                            
                                <button class="button btn-danger"  style="font-weight:bold;" type="button"><i class="fa fa-minus"></i>Remove</button>
                            </div>
                        </div>
                    </div> -->
                </div><br>
                <button class="button">Submit</button>
                </form>
            </div>
        </div>
    </div>
  </section>
  <script src="/Public/assets/js/jquery-2.2.4.min.js" type="text/javascript"></script>
  <script src="/Public/asset/js/app.js"></script>
<!-- <script src="/Public/assets/js/bootstrap.min.js" type="text/javascript"></script> -->
  <script type="text/javascript">

$(document).ready(function() {

  $(".btn-success").click(function(){ 
      var html = $(".clone").html();
      $(".increment").after(html);
  });

  $("body").on("click",".btn-danger",function(){ 
      $(this).parents(".control-group").remove();
  });

});

</script>
  </body>
</html>