
<!DOCTYPE html>
<html>  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Businesses</title>
    <link href="../Public/assets/css/bulma.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    
  <style>
    a:hover{
      text-decoration: none;
    }
    body{
    /* background:url('/Public/assets/img/auth2.jpg'); */
    background:linear-gradient(rgba(255,255,258,0.5),rgba(255,255,255,3.20)),url('/Public/img/bac12.jpg');

    background-size:cover;
    background-repeat:no-repeat;
    height:100%;
    background-attachment: fixed;
    }
    header nav{
      box-shadow:4px 4px 40px black;
    }
    section{
      /* text-align:center; */
      padding:50px;
    }
    section .h1{
      max-width:85%;
      font-size:50px;
      padding:10px;
      font-weight:bolder;
    }
    section .h3{
      color:white;
      max-width:85%;
      font-size:30px;
      padding:10px;
      font-weight:bolder;
    }
    .row{
        display:grid;
        grid-template-columns:repeat(auto-fill,minmax(300px,1fr));
        grid-gap:10px;
    }
    .h2{
        font-size:30px;
    }
    small{
        text-align:left;
        color:black;
    }
    .box-header{
        display:grid;
        grid-template-columns:auto auto;
    }
    figure img{
        border-radius:40px;
        /* box-shadow:4px 2px 10px black; */
    }

    .extra{
        cursor:pointer;

    }
    .ext small{
        color:silver;
    }
    .scrolling-wrapper {

      display: flex;
      flex-wrap: nowrap;
      overflow-x: auto;
      -webkit-overflow-scrolling: touch;
      scroll-behavior: smooth;
      transition: .8s;
  
}
.scrolling-wrapper::-webkit-scrollbar{
  display:none;
  transition: .8s;
 
}
.cards {
    
    transition: .8s;
    
    margin:10px;
  }
.card{

 
  
  box-shadow:0 2px 3px rgba(10,10,10,.1),0 0 0 1px rgba(10,10,10,.1);
  color:#4a4a4a;
  transition: .8s;
  /* padding:0px; */
  /* margin:10px; */

  width:100%;
 

    /* position: relative; */
}
.scrolling-wrapper .cards header.card-header p{
  /* display:none; */
  position:absolute;
  /* box-shadow:4px 0px 6px black;  */
  color:white;
  margin:auto;
  text-align:center;
  left:1px;
  top:50px;
  background:silver;
  padding:10px;
  border-top-right-radius:20px;
  border-bottom-right-radius:20px;
}
.scrolling-wrapper .cards header.card-header small{
  /* display:none; */
  position:absolute;
  /* box-shadow:4px 0px 6px black;  */
  color:green;
  font-weight:bolder;
  margin:auto;
  text-align:center;
  left:303px;
  top:50px;
  background:silver;
  padding:20px;
  font-size:15px;
  border-top-right-radius:0px;
  border-bottom-right-radius:0px;
}
.scrolling-wrapper .cards header.card-header img:hover p{
  display:block;
}   
  </style>

  </head>
  <body>
    <?php view('Layouts/header'); ?>
    <br><br><br>
    <?php view('Layouts/Validation'); ?>
    <section>
      
      <h1 style="color:white;text-shadow:3px 5px 4px black;" class="h1">BUSINESSES</h1>
      <?php if(count($businesses) >= 1): ?>
        <div class="scrolling-wrapper">
        <?php foreach($businesses as $business): ?>
        <?php 
          $query = require 'core/bootstrap.php';
          $view = $query->selectWhere('views','business_name',$business->name);
          $views = count($view);
          
        ?>
          <div style="cursor:move;" class="cards">
            <div class="card" style="width:380px;">
              <header class="card-header" style="text-align:center;">
                <small class="button is-danger is-rounded is-small"><?php echo $views; ?> <i style="font-size:20px;margin-left:3px;" class="fa fa-eye"></i></small>
                <p id="img-display" style=""><a href="/admin/business/image/add?id=<?php echo $business->id; ?>"><i class="fa fa-plus"></i></a></p>
                <img onMouseover="showP()" style="cursor:zoom-in;height:300px;width:400px;border:2px dotted green;" src="../Public/uploads/img/<?php echo $business->display_image; ?>">
              </header>
              <div class="card-content">
                <div class="content">
                
                <h1 class="h2"><?php echo $business->name ?></h1>
                
                <hr>
                <small><?php echo $business->about ?></small>
                  <hr>
                  <b><p>Categories: </p></b>
                  <?php
                    $query = require 'core/bootstrap.php';
                    $id = $business->id;
                    $categories = $query->selectJoin($business->id);
                    foreach($categories as $category):
                  ?>
                  <small style="border-right:1px solid silver;padding:4px;" class="small"><?php echo $category->category;?></small>
                    <?php endforeach; ?>
                  <br>
                  <b><p>Location: </p></b>
                  <small>
                    <?php echo 'No '.$business->street_number.', '.$business->street_name.', '.$business->city.', '.$business->country ?>.
                  </small>
                  <b><p>Phone:</p></b>
                  <small><?php echo $business->phone ?></small>
                  <b><p>Email Address: </p></b>
                  <small><?php echo $business->email ?></small>

                </div>
              </div>
              <footer class="card-footer">
                <a href="/admin/businesses/edit?no=<?php echo $business->id; ?>" class="card-footer-item">Edit</a>
                <a href="/admin/businesses/add_category?no=<?php echo $business->id; ?>" class="card-footer-item">Add Category</a>
                <a href="/admin/businesses/delete?no=<?php echo $business->id; ?>" class="card-footer-item">Delete</a>
              </footer>
            </div>
          </div>
        <?php endforeach; ?>
        </div>
      <?php  else: ?>
      <h1 class="h1">No Business Registered Yet!. </h1>
      <?php endif ?>
    </section>
   <?php view('Layouts/footer'); ?>
  </body>
</html>