<!DOCTYPE html>
<html>  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Business Directory</title>
    <link href="/Public/assets/css/bulma.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <link rel="stylesheet" href="/Public/assets/css/animate.css">
    <style>
    a:hover{
      text-decoration: none;
    }
    body{
    /* background:url('/Public/assets/img/auth2.jpg'); */
    background-size:cover;
    background-repeat:no-repeat;
    height:800px;
    background-attachment: fixed;
    }
    header nav{
      box-shadow:4px 4px 40px pink;
      background:linear-gradient(rgba(255,255,255,0.5),rgba(255,255,255,3.20)),url('/Public/img/bac3.jpg');
      
    }
    section{
      /* text-align:center; */
      padding:50px;
    }
    section .h1{
      max-width:85%;
      font-size:50px;
      padding:10px;
      font-weight:bolder;
    }
    section .h3{
      color:white;
      max-width:85%;
      font-size:30px;
      padding:10px;
      font-weight:bolder;
    }
    section .container{
        width:50%;
    }
    .box{
      box-shadow:4px 4px 40px pink;
    }
  </style>
  </head>
  <body>
    <?php view('Layouts/header');?>
    <section class="section">
        <div class="container">
        <br><br><br><br><br><br>
            <div class="box animated fadeInRight">
                <h1 class="title">Create New Category</h1>
                <hr>
                <?php view('Layouts/Validation');?>
                <form action="/admin/categories" method="post">
                    <label for="" class="label">Category's Name</label>
                    <input class="input" type="text" placeholder="Category's Name" name="category">
                    <br><br>
                    <button class="button is-link">Create</button>
                    
                    
                </form>
            </div>
        </div>
    </section>
  </body>
</html>