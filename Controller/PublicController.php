<?php

class PublicController{
    public function index(){
        return view('PublicPage/index');
    }
    public function search(){
        $search = $_REQUEST['search'];
        $query = require 'core/bootstrap.php';
        $results = $query->selectLike('businesses',[
            'name' => $search,
            'email' => $search,
            'phone' => $search,
            'about' => $search,
            'street_name' => $search,
            'country' => $search
        ]);
        return view('PublicPage/search',compact('results', 'search'));
    }
    public function result(){
        $search = $_REQUEST['name'];
        $query = require 'core/bootstrap.php';
        $query->insert('views',[
            'business_name' => $search
        ]);
        $results = $query->selectLike('businesses',[
            'name' => $search,
        ]);
        $view = $query->selectAll('views',[
                    'business_name' => $search
                 ]);
        $views = count($view);
        $ids = $query->show('businesses','name',$search);
        foreach($ids as $i){
            $id = $i;
        }
        $images = $query->selectImageJoin($id->id);
        return view('PublicPage/result',compact('results','images','views'));
    }
    
}